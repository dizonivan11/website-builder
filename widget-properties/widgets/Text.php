<div class="container">
    <div class="row">
        <div class="col-12">
            <label class="widget-properties-label">Content</label>
            <textarea name="text-content" style="width: 100%; height: 200px; resize: none;"></textarea>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12">
            <label class="widget-properties-label">Text Style</label><br>
            <input type="checkbox" name="text-bold" value="Bold">Bold<br>  
            <input type="checkbox" name="text-italicized" value="Italicized">Italicized<br>
            <input type="checkbox" name="text-underlined" value="Underlined">Underlined<br>
        </div>
    </div>
</div>