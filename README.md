# website-builder
**Technologies Used: PHP, MYSQL, Medoo**
***
**TODO List:**
<ol>
<li>Color Picker</li>
<li>Widget Background</li>
<li>Applying Widget Property changes</li>
<li>Global Settings</li>
<li>Adding Rows</li>
<li>Deleting Rows</li>
<li>Sorting Rows</li>
<li>Adding Columns</li>
<li>Site Map</li>
<li>Adding Pages</li>
<li>Sorting Pages</li>
<li>Deleting Pages</li>
<li>Uploading Images</li>
<li>Previewing</li>
<li>Publishing</li>
</ol>
